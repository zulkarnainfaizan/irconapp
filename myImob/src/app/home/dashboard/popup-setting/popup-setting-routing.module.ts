import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopupSettingPage } from './popup-setting.page';

const routes: Routes = [
  {
    path: '',
    component: PopupSettingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopupSettingPageRoutingModule {}
