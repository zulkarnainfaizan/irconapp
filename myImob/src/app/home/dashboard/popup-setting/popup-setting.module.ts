import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopupSettingPageRoutingModule } from './popup-setting-routing.module';

import { PopupSettingPage } from './popup-setting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopupSettingPageRoutingModule
  ],
  declarations: [PopupSettingPage]
})
export class PopupSettingPageModule {}
