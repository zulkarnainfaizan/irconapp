import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popup-setting',
  templateUrl: './popup-setting.page.html',
  styleUrls: ['./popup-setting.page.scss'],
})
export class PopupSettingPage implements OnInit {

  constructor(public popover: PopoverController) { }

  ngOnInit() {
  }
  closePopup(){
    this.popover.dismiss();
  }

}
