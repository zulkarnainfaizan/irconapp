import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopupSettingPage } from './popup-setting.page';

describe('PopupSettingPage', () => {
  let component: PopupSettingPage;
  let fixture: ComponentFixture<PopupSettingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupSettingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopupSettingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
