// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-dashboard',
//   templateUrl: './dashboard.page.html',
//   styleUrls: ['./dashboard.page.scss'],
// })
// export class DashboardPage implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { PagesPage } from 'src/app/pages/pages.page';
import { PopupSettingPageModule } from './popup-setting/popup-setting.module';
import { PopupSettingPage } from './popup-setting/popup-setting.page';
// import { PopupModule } from '../popup/popup.module';
// import { PopoverComponent } from 'src/app/component/popover/popover.component';
// import { PopupComponent } from '../popup/popup.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  constructor(public router: Router, public popoverController: PopoverController) { 
    // console.log(this.router.getCurrentNavigation().extras.state.token);
  }
value = 0;
    // document.body.appendChild(popoverElement);
    //  popoverElement.present();
    async presentPopover(ev: Event) {
      const popover = await this.popoverController.create({
        // component: PagesPage,
        component: PopupSettingPage,
     
        event: ev,
        translucent: true
      });
      // popover.present(); 
      return await popover.present();
    }

  ngOnInit() {
  }

}
