import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { PagesPage } from 'src/app/pages/pages.page';
import { PagesPageModule } from 'src/app/pages/pages.module';
import { PopupSettingPageModule } from './popup-setting/popup-setting.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule, PopupSettingPageModule
  ],
  // entryComponents:[],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
