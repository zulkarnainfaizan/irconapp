//material.module.ts

import { NgModule } from '@angular/core';

import {
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatCheckboxModule,
  MatMenuModule,
  MatSelectModule,
  MatRadioModule,
  MatGridListModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatStepperModule,
  MatButtonToggleModule,
  MatExpansionModule,
  MatSlideToggleModule,
  MatSidenavModule,
  MatListModule,
  MatBadgeModule,
  MatProgressBarModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
@NgModule({

  exports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    MatGridListModule,
    MatToolbarModule,
    MatGridListModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatStepperModule,
    MatButtonToggleModule,
    MatExpansionModule,
    MatListModule,
    MatSlideToggleModule,
    NgxMatSelectSearchModule,
    MatToolbarModule,
    LayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatBadgeModule,
    MatProgressBarModule

  ]
})

export class MaterialModule { }
