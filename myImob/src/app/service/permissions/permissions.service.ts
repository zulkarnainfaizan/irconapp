import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  constructor(private genericService: GenericService) {

  }
  getPermissions(model) {
    // moduleId: any, roleId: any, departmentId
    return this.genericService.post('api/permissions/get', model);
  }

  saveModuleRole(model) {
    //hasAccess: boolean, moduleId: any, roleId: any, departmentId
    return this.genericService.post('api/permissions/saveModuleRole', model);
  }
  saveModuleRoleOperation(model) {
    return this.genericService.post('api/permissions/SaveModuleRoleOperation', model);
  }
}
