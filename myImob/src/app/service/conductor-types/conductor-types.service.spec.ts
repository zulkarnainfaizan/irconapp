import { TestBed } from '@angular/core/testing';

import { ConductorTypesService } from './conductor-types.service';

describe('ConductorTypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConductorTypesService = TestBed.get(ConductorTypesService);
    expect(service).toBeTruthy();
  });
});
