import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ConductorTypesService {

  constructor(private genericService: GenericService) { }

  getConductorTypes(tableOptions: any, antiCreep: boolean) {
    return this.genericService.post('api/conductortype/get?IsAntiCreep=' + antiCreep, tableOptions);
  }

  getConductorTypeDetails(id: any) {
    return this.genericService.get('api/conductortype/' + id);
  }
}
