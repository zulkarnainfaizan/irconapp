import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class OperationsService {


  constructor(private genericService: GenericService) { }
  add(model: any) {
    return this.genericService.post('api/operations/add', model);
  }

  getoperationsitems(tableOptions: any) {
    return this.genericService.post('api/operations/get', tableOptions);
  }

  getoperationsItemDetails(id: any) {
    return this.genericService.get('api/operations/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/operations/' + model.id, model);
  }

  deleteOperationsitem(id: any) {
    return this.genericService.patch('api/operations/' + id, {});
  }
}
