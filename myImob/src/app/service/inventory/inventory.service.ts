import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private genericService: GenericService) { }
  add(model: any) {
    return this.genericService.post('api/inventoryitem/add', model);
  }

  getinventoryitems(tableOptions: any) {
    return this.genericService.post('api/inventoryitem/get', tableOptions);
  }

  getinventoryItemDetails(id: any) {
    return this.genericService.get('api/inventoryitem/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/inventoryitem/' + model.id, model);
  }

  deleteInventoryitem(id: any) {
    return this.genericService.patch('api/inventoryitem/' + id, {});
  }
}
