import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private genericService: GenericService) {

  }
  getModuleList(tableOptions: any) {
    return this.genericService.post('api/module/get', tableOptions);
  }
}
