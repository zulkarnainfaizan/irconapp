
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 4000
});

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

  constructor(private router: Router) { }

  // success(message: any, route: any) {
  //   Swal.fire({
  //     title: 'Success',
  //     text: message,
  //     type: 'success',
  //     width: 500,
  //     allowOutsideClick: false,
  //     allowEscapeKey: false,
  //     showCancelButton: false,
  //     confirmButtonText: 'OK',
  //     cancelButtonText: "Cancel",
  //     animation: true,
  //   }).then((result) => {
  //     this.router.navigate([route]);
  //   })
  // }

  // successMessage(message: any) {
  //   Swal.fire({
  //     title: 'Success',
  //     text: message,
  //     type: 'success',
  //     width: 500,
  //     allowOutsideClick: false,
  //     allowEscapeKey: false,
  //     showCancelButton: false,
  //     confirmButtonText: 'OK',
  //     cancelButtonText: "Cancel",
  //     animation: true,
  //   }).then((result) => {
  //   })
  // }

  // info(message: any) {
  //   Swal.fire({
  //     title: 'Information',
  //     text: message,
  //     type: 'info',
  //     width: 500,
  //     allowOutsideClick: false,
  //     allowEscapeKey: false,
  //     showCancelButton: false,
  //     confirmButtonText: 'OK',
  //     cancelButtonText: "Cancel",
  //     animation: true,
  //   }).then((result) => {

  //   })
  // }

  // error(message: any) {
  //   Swal.fire({
  //     title: 'Error',
  //     text: message,
  //     type: 'error',
  //     width: 500,
  //     allowOutsideClick: false,
  //     allowEscapeKey: false,
  //     showCancelButton: false,
  //     confirmButtonText: 'OK',
  //     cancelButtonText: "Cancel",
  //     animation: true,
  //   }).then((result) => {

  //   })
  // }

  showLoader() {
    Swal.fire({
      title: 'Processing',
      html: 'Please wait ...',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      }
    }).then((result) => {

    })
  }
  hidePopup() {
    Swal.close();    
  }

  viewImage(imageURL, originalFileName, size) {
    Swal.fire({
      html: originalFileName + "<br>" + size,
      imageUrl: imageURL,
      imageAlt: 'Custom image',
      confirmButtonText: 'Close',
      confirmButtonColor: '#f96565',
      allowOutsideClick: false,
      allowEscapeKey: false,
      animation: false
    });
  }

  customHtml(title, html: any) {
    Swal.fire({
      title: title,
      //type: 'info',
      html: html,
      width: 500,
      allowOutsideClick: false,
      allowEscapeKey: false,
      showCancelButton: false,
      confirmButtonText: 'Close',
      confirmButtonColor: '#f96565'
    }).then((result) => {

    })
  }

  // uploadInfo(message: any) {
  //   Swal.fire({
  //     type: 'info',
  //     html: message,
  //     width: 500,
  //     allowOutsideClick: false,
  //     allowEscapeKey: false,
  //     showCancelButton: false,
  //     confirmButtonText: 'Close',
  //     confirmButtonColor: '#f96565'
  //   }).then((result) => {

  //   });
  // }

}
