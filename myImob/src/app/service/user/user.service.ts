import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  $changeRole = new Subject<any>();
  constructor(private genericService: GenericService) { }

  add(model: any) {
    return this.genericService.post('api/user/add', model);
  }

  getUsers(tableOptions: any) {
    return this.genericService.post('api/user/get', tableOptions);
  }
  getUserDetails(id: any) {
    return this.genericService.get('api/user/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/user/Edit', model);
  }
  changeRole(model: any) {
    return this.genericService.put('api/user/ChangeRole', model);
  }


  changeStatus(model: any) {
    return this.genericService.put('api/user/ChangeStatus', model);
  }

  delete(id: any) {
    return this.genericService.patch('api/user/' + id, {});
  }
}
