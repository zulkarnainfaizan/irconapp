import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyProfileService {
  editProfile = new Subject<any>();
  profileAdded = new Subject<any>();


  constructor(private genericService: GenericService) { }
  add(model: any) {
    return this.genericService.post('api/myProfile/add', model);
  }
 /* getmyprofile(tableOptions: any) {
  * return this.genericService.post('api/myProfile/get', tableOptions);
  * }*/
  getmyprofile(id: any) {
    return this.genericService.get('api/user/' + id);
  }
 
  update(model: any) {
    return this.genericService.put('api/user/UpdateMyProfile', model);
  }

  deletemyprofile(id: any) {
    return this.genericService.patch('api/myProfile/' + id, {});
  }
}
