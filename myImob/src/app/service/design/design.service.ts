import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class DesignService {

  constructor(private genericService: GenericService) { }

  getDesigns(tableOptions: any,workType:any) {
    return this.genericService.post('api/design/get?workType=' + workType, tableOptions);
  }

  add(model: any) {
    return this.genericService.post('api/design/add', model);
  }

  getDesignDetails(id: any) {
    return this.genericService.get('api/design/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/design/' + model.id, model);
  }

  deleteDesign(id: any) {
    return this.genericService.patch('api/design/' + id, {});
  }

  GetDetailsFromWorkSection(model: any) {
    return this.genericService.post('api/design/GetDetailsFromWorkSection', model);
  }

  getFolderList(tableOptions: any, id: any) {
    return this.genericService.post('api/design/GetFolderList/' + id, tableOptions);
  }
}
