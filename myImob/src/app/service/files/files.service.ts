import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilesService {
  uploadResourceUpdated = new Subject<boolean>();
  constructor(private genericService: GenericService) { }

  getAttachmentsList(tableOptions: any, id: any) {
    return this.genericService.post('api/FileManager/GetFileList/' + id, tableOptions);
  }
  getFolderList(tableOptions: any, id: any) {
    return this.genericService.post('api/FileManager/GetFolderList/' + id, tableOptions);
  }
  downloadAttachment(id: any) {
    return this.genericService.getDownloads('api/FileManager/Download/' + id);
  }
  UploadAttachments(tableOptions, folderId) {
    return this.genericService.post('api/FileManager/upload/' + folderId, tableOptions);
  }
}
