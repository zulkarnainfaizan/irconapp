import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Http, Headers, RequestOptions, Response, ResponseContentType } from '@angular/http';
import { AuthUtil } from 'src/app/global-values/auth-util';

@Injectable({
    providedIn: 'root'
})
export class GenericService {
    baseURL: string = '';
    constructor(private httpClient: HttpClient, private http: Http) {
        // this.baseURL = baseUrl
        this.baseURL = 'http://dev1.re-usbrl.com/'
    }

    post(url: string, data: any) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': AuthUtil.getTokenForHeader()
            })
        };
        return this.httpClient.post(this.baseURL + url, data, httpOptions);
    }

    get(url: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Cache-Control': 'no-cache',
                'Pragma': 'no-cache',
                'Content-Type': 'application/json',
                'Authorization': AuthUtil.getTokenForHeader()

            })
        };
        return this.httpClient.get(this.baseURL + url, httpOptions);
    }

    put(url: string, data: any) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': AuthUtil.getTokenForHeader()
            })
        };

        return this.httpClient.put(this.baseURL + url, data, httpOptions);
    }

    patch(url: string, data: any) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': AuthUtil.getTokenForHeader()
            })
        };
        return this.httpClient.patch(this.baseURL + url, data, httpOptions);
    }

    getDownloads(url: string) {
        const headers = new Headers({
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache',
            'Content-Type': 'application/json',
            'Authorization': AuthUtil.getTokenForHeader()
        });
        const options = new RequestOptions({ headers: headers });
        options.responseType = ResponseContentType.Blob;

        return this.http.get(url, options);
    }

}
