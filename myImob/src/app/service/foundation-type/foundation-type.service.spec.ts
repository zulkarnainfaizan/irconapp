import { TestBed } from '@angular/core/testing';

import { FoundationTypeService } from './foundation-type.service';

describe('FoundationTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FoundationTypeService = TestBed.get(FoundationTypeService);
    expect(service).toBeTruthy();
  });
});
