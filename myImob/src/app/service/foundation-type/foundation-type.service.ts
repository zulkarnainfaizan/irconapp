import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class FoundationTypeService {

  constructor(private genericService: GenericService) { }

  getFoundationTypes(tableOptions: any, isChart: boolean) {
    return this.genericService.post('api/foundationtype/get?isChart=' + isChart, tableOptions);
  }

  getFoundationTypeDetails(id: any) {
    return this.genericService.get('api/foundationtype/' + id);
  }

  getMastTypes(tableOptions: any) {
    return this.genericService.post('api/masttype/get', tableOptions);
  }

  getMastTypeDetails(id: any) {
    return this.genericService.get('api/masttype/' + id);
  }

}
