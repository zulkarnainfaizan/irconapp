import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
import { Subject } from 'rxjs';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  supplierAdded = new Subject<any>();
  editSupplier = new Subject<any>();
  constructor(private httpClient: HttpClient, private genericService: GenericService) { }

  add(model: any) {
    return this.genericService.post('api/supplier/add', model);
  }

  getSuppliers(tableOptions: any) {
    return this.genericService.post('api/supplier/get', tableOptions);
    //return this.httpClient.get("./assets/json/suppliers.json");
  }

  getSupplierDetails(id: any) {
    return this.genericService.get('api/supplier/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/supplier/' + model.id, model);
  }

  deleteSupplier(id: any) {
    return this.genericService.patch('api/supplier/' + id, {});
  }
}
