import { TestBed } from '@angular/core/testing';

import { CantileverService } from './cantilever.service';

describe('CantileverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CantileverService = TestBed.get(CantileverService);
    expect(service).toBeTruthy();
  });
});
