import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
@Injectable({
  providedIn: 'root'
})
export class ProgressService {

  constructor(private genericService: GenericService) { }
  getProgress(tableOptions: any, workType: any) {
    return this.genericService.post('api/workprogress/get?workType=' + workType, tableOptions);
  }

  add(model: any) {
    return this.genericService.post('api/workprogress/add', model);
  }

  getProgressDetails(id: any) {
    return this.genericService.get('api/workprogress/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/workprogress/' + model.id, model);
  }

  deleteProgress(id: any) {
    return this.genericService.patch('api/workprogress/' + id, {});
  }

  getProgressPercentage(workType: any, departmentId: any) {
    return this.genericService.get('api/workprogress/GetOnHoldPercentage/' + workType + '/' + departmentId);
  }
}
