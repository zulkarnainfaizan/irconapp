import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MailSettingsService {

  constructor(private genericService: GenericService) { }

  testMail(model: any) {
    return this.genericService.put('api/mail/testMail', model)
  }
  getmailsettings(id: any) {
    return this.genericService.get('api/mailsettings/' + id);
  }
}
