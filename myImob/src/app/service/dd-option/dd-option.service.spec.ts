import { TestBed } from '@angular/core/testing';

import { DdOptionService } from './dd-option.service';

describe('DdOptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DdOptionService = TestBed.get(DdOptionService);
    expect(service).toBeTruthy();
  });
});
