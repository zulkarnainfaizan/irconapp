import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
@Injectable({
  providedIn: 'root'
})
export class DdOptionService {

  constructor(private genericService: GenericService) { }

  getDDOptions(tableOptions: any, dType: string) {
    return this.genericService.post('api/ddoption/get?dType=' + dType, tableOptions);
  }
}
