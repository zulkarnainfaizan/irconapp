import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService {


  constructor(private genericService: GenericService) { }
  add(model: any) {
    return this.genericService.post('api/role/add', model);
  }

  getroleitems(tableOptions: any) {
    return this.genericService.post('api/role/get', tableOptions);
  }

  getroleItemDetails(id: any) {
    return this.genericService.get('api/role/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/role/' + model.id, model);
  }

  deleteRoleitem(id: any) {
    return this.genericService.patch('api/role/' + id, {});
  }
}
