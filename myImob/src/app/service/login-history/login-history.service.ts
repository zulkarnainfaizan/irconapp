import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class LoginHistoryService {

  constructor(private genericService: GenericService) { }
  add(model: any) {
    return this.genericService.post('api/loginState/add', model);
  }

  getloginhistory(tableOptions: any) {
    return this.genericService.post('api/loginState/get', tableOptions);
  }

  update(model: any) {
    return this.genericService.put('api/loginState/' + model.id, model);
  }

  deleteloginhistory(id: any) {
    return this.genericService.patch('api/loginState/' + id, {});
  }
}
