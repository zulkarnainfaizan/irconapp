import { TestBed } from '@angular/core/testing';

import { LoginHistoryService } from './login-history.service';

describe('LoginHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginHistoryService = TestBed.get(LoginHistoryService);
    expect(service).toBeTruthy();
  });
});
