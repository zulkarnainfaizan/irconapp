import { Injectable } from "@angular/core";
import { tap } from "rxjs/operators";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private router: Router) { }
    //function which will be called for all http calls
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                event => {
                    //logging the http response to browser's console in case of a success
                    if (event instanceof HttpResponse) {
                       // console.log("api call success :", event);
                       
                    }
                },
                error => {
                    //logging the http response to browser's console in case of a failuer
                    if (error && error.status == 401) {
                        localStorage.clear();
                        sessionStorage.clear();
                        localStorage.setItem('AccessDeniedError', JSON.stringify(error));
                        var isToken: boolean = localStorage.getItem('token') ? true : false;
                        if (isToken) {
                            this.router.navigate(['/access-denied']);
                            error.message = 'Access denied'
                        } 
                        
                    }
                    else if (event instanceof HttpResponse) {
                        console.log("api call error :", event);
                    }
                }
            )
        );
    }
}
