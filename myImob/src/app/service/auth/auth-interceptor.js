"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var operators_1 = require("rxjs/operators");
var http_1 = require("@angular/common/http");
var CustomInterceptor = /** @class */ (function () {
    function CustomInterceptor(router) {
        this.router = router;
    }
    //function which will be called for all http calls
    CustomInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).pipe(operators_1.tap(function (event) {
            //logging the http response to browser's console in case of a success
            if (event instanceof http_1.HttpResponse) {
                //console.log("api call success :", event);
            }
        }, function (error) {
            //logging the http response to browser's console in case of a failuer
            if (error && error.status == 401) {
                localStorage.clear();
                sessionStorage.clear();
                localStorage.setItem('AccessDeniedError', JSON.stringify(error));
                _this.router.navigate(['/access-denied']);
                error.message = 'Access denied';
            }
            else if (event instanceof http_1.HttpResponse) {
                console.log("api call error :", event);
            }
        }));
    };
    return CustomInterceptor;
}());
exports.CustomInterceptor = CustomInterceptor;
//# sourceMappingURL=auth-interceptor.js.map