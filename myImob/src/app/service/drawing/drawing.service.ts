import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class DrawingService {

  constructor(private genericService: GenericService) { }
  getDrawings(tableOptions: any) {
    return this.genericService.post('api/Drawing/get', tableOptions);
  }
  add(model: any) {
    return this.genericService.post('api/Drawing/add', model);
  }


  deleteDrawing(id: any) {
    return this.genericService.patch('api/Drawing/' + id, {});
  }

  update(model: any) {
    return this.genericService.put('api/Drawing/' + model.id, model);
  }

  getDrawingDetails(id: any) {
    return this.genericService.get('api/Drawing/' + id);
  }

  getStatusesOfDrawing(id: any,data:any) {
    return this.genericService.post('api/DrawingStatus/GetStatusesOfDrawing/?drawingId=' + id, data);
  }

  getDrawingStatusList(tableOptions: any,statusId:any) {
    return this.genericService.post('api/DrawingStatus/GetAll/?statusId=' + statusId, tableOptions);
  }
  updateDrawingStatus(tableOptions: any) {
    return this.genericService.post('api/drawingStatus/save', tableOptions);
  }
}
