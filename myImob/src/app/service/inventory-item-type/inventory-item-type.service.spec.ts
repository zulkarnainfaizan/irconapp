import { TestBed } from '@angular/core/testing';

import { InventoryItemTypeService } from './inventory-item-type.service';

describe('InventoryItemTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InventoryItemTypeService = TestBed.get(InventoryItemTypeService);
    expect(service).toBeTruthy();
  });
});
