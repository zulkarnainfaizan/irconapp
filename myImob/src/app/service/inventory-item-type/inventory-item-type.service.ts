import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class InventoryItemTypeService {

  constructor(private genericService: GenericService) { }
  add(model: any) {
    return this.genericService.post('api/inventoryitemtype/add', model);
  }

  getInventoryItemTypes(tableOptions: any) {
    return this.genericService.post('api/inventoryitemtype/get', tableOptions);
  }

  getinventoryItemTypeDetails(id: any) {
    return this.genericService.get('api/inventoryitemtype/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/inventoryitemtype/' + model.id, model);
  }

  deleteInventoryItemType(id: any) {
    return this.genericService.patch('api/inventoryitemtype/' + id, {});
  }
}
