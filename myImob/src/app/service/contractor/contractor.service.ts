import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ContractorService {

  constructor(private genericService: GenericService) { }

  add(model: any) {
    debugger;
    return this.genericService.post('api/contractor/add', model);
  }

  getcontractoritems(tableOptions: any) {
    return this.genericService.post('api/contractor/get', tableOptions);
  }

  getcontractorItemDetails(id: any) {
    return this.genericService.get('api/contractor/' + id);
  }

  update(model: any) {
    return this.genericService.put('api/contractor/' + model.id, model);
  }

  deleteContractoritem(id: any) {
    return this.genericService.patch('api/contractor/' + id, {});
  }
}
