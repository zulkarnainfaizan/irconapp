import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class StationService {

  constructor(private httpClient: HttpClient, private genericService: GenericService) { }
  getAllStations(model: any) {

    return this.genericService.post('api/station/get', model);
    //return this.httpClient.get("./assets/json/stations.json");
  }
}
