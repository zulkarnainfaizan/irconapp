import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

    constructor(private genericService: GenericService) { }

  getDepartments(tableOptions: any) {
    return this.genericService.post('api/department/get', tableOptions);
  }
}
