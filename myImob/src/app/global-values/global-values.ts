export const globalValues = {
  chain: 49.71
};
export class GlobalUtil {
  public static setCurrentDepartment(department: any) {
    localStorage.setItem('currentDepartment', JSON.stringify(department));
  }
  public static getCurrentDepartment() {
    return JSON.parse(localStorage.getItem('currentDepartment'));
  }

  public static getAllDepartments() {
    return JSON.parse(localStorage.getItem('allDepartments'));
  }

  public static setAllModules(modules: any) {
    localStorage.setItem('allModules', JSON.stringify(modules));
    var modulesKeyed = {}
    if (modules && modules.length) {
      for (var i = 0; i < modules.length; i++) {
        modulesKeyed[modules[i].controller] = modules[i];
      }
    }
    localStorage.setItem('allModulesKeyed', JSON.stringify(modulesKeyed));
  }
  public static getAllModules() {
    return JSON.parse(localStorage.getItem('allModules'));
  }

  public static getModuleFromControl(controller) {
    var modulesKeyed = JSON.parse(localStorage.getItem('allModulesKeyed'));
    if (modulesKeyed) {
      return modulesKeyed[controller];
    }
    else return null;

  }

  public static getFormData(object: any) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }

  static getPermissionsForModule(controller) {
    var allPerms = JSON.parse(localStorage.getItem('permissions') || null);
    if (allPerms && allPerms[controller] && allPerms[controller].ops) {
      return allPerms[controller].ops;
    }
    else {
      return null;
    }
  }
}
