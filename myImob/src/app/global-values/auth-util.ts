import { GlobalUtil } from './global-values';

export class AuthUtil {

  static login(resp) {
    if (resp) {
      this.setToken(resp.token);
      this.setUserInfo(resp.user);
      this.setPermissions(resp.permissions);
      if (resp.user) {
        let depts = resp.user.departments;
        this.setAllDepartments(depts);
        if (depts && depts.length)
          GlobalUtil.setCurrentDepartment(depts[0]);
      }
    }
  }
  static setToken(token) {
    localStorage.setItem('token', token);
  }
  static getToken(): string {
    return localStorage.getItem('token');
  }
  static setUserInfo(userObj: any) {
    localStorage.setItem('user', JSON.stringify(userObj));
  }
  static getUserInfo(): any {
    return JSON.parse(localStorage.getItem('user') || null);
  }

  static getTokenForHeader(): string {
    return 'Bearer ' + localStorage.getItem('token');
  }
  static logout() {
    localStorage.clear();
  }
  static isLoggedIn() {
    return this.getToken() ? true : false;
  }
  static getPermissions() {
    return JSON.parse(localStorage.getItem('permissions') || null);
  }
  static setPermissions(permissions) {
    var perms: any = null;
    if (permissions && permissions.length) {
      perms = {};
      for (let index = 0; index < permissions.length; index++) {
        let controller = permissions[index].controller;
        perms[controller] = permissions[index];
        if (perms[controller] && perms[controller].operations) {
          perms[controller].ops = {};
          for (let index2 = 0; index2 < perms[controller].operations.length; index2++) {
            perms[controller].ops[perms[controller].operations[index2]] = true;
          }

        }
      }
    }
    localStorage.setItem('permissions', JSON.stringify(perms));
  }

  public static setAllDepartments(departments: any) {
    localStorage.setItem('allDepartments', JSON.stringify(departments));
  }
}
