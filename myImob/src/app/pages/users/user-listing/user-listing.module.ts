import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserListingPageRoutingModule } from './user-listing-routing.module';
import { MatButtonModule, MatCardModule, MatTabsModule, MatChipsModule, MatIconModule, MatToolbarModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule } from "@angular/material";
 

import { UserListingPage } from './user-listing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserListingPageRoutingModule,
    /////mymodules
    MatToolbarModule,
    MatIconModule
  ],
  declarations: [UserListingPage]
})
export class UserListingPageModule {

}
