  // import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-add-user',
//   templateUrl: './add-user.page.html',
//   styleUrls: ['./add-user.page.scss'],
// })
// export class AddUserPage implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { Subject, ReplaySubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { RoleService } from '../../../service/role/role.service';
import { AlertifyService } from '../../../service/alerity/alertify.service';
import { UserService } from 'src/app/service/user/user.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { DepartmentService } from '../../../service/department/department.service';
import { ContractorService } from '../../../service/contractor/contractor.service';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss']
})
export class AddUserPage implements OnInit {
  @ViewChild('addUserForm', { static: false }) public addUserForm: NgForm;
  user: any = {};
  genders: any[] = [];
  form: FormGroup;
  id: any;
  operation: any;
  roleData: any[] = [];
  departmentData: any[] = [];
  contractorData: any[] = [];

  /** control for the MatSelect filter keyword multi-selection */
  public roleMultiFilterCtrl: FormControl = new FormControl();
  public departmentMultiFilterCtrl: FormControl = new FormControl();
  public contractorMultiFilterCtrl: FormControl = new FormControl();

  tableOptions: any = {
    page: 1,
    rows: 15,
    isSearch: false,
    searchKeyword: "",
    SortCol: "Name",
    IsAscending: false,
  }

  /** list of roles */


  /** list of roles filtered by search keyword for multi-selection */
  public filteredRolesMulti: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredDepartmentMulti: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredContractorsMulti: ReplaySubject<any[]> = new ReplaySubject<any[]>(1); 

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(
    private _formBuilder: FormBuilder,
    private roleService: RoleService,
    private alertifyService: AlertifyService,
    private userService: UserService,
    private departmentService: DepartmentService,
    private location: Location,
    private route: ActivatedRoute,
    private contractorService: ContractorService
  ) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      id: null,
      name: ['', Validators.required],
      email: ['', Validators.email],
      phone: ['', Validators.required],
      designation: ['', Validators.required],
      roleIds: ['', Validators.required],
      departmentIds: ['', Validators.required],
      address: ['', Validators.required],
      gender: ['', Validators.required],
      contractorIds: ['', Validators.required]
    });
    this.getGender();
    this.getroleitems();
    this.getDepartments();
    this.getcontractoritems();
    this.operation = 'Add'
    this.id = this.route.snapshot.paramMap.get("id");
    if (this.id) {
      this.operation = 'Edit'
      this.getUserDetails()
    }

    // listen for search field value changes
    this.roleMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.filterRolesMulti();
      });
    this.contractorMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.filterContractorsMulti();
      });
    this.departmentMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.filterDepartmentMulti();
      });


  }
  getGender() {
    this.genders = [{ 'id': 'male', value: 'Male' }, { 'id': 'female', value: 'Female' }]

  }
  getroleitems() {
    this.roleService.getroleitems(this.tableOptions).pipe(takeUntil(this.ngUnsubscribe)).subscribe((res: any) => {
      if (res) {
        this.roleData = res;
      }
      if (this.roleData && this.roleData.length)
        this.filteredRolesMulti.next(this.roleData.slice());
    }, error => {
      console.log(error);
    });
  }
  //contractor
  getcontractoritems() {
    this.contractorService.getcontractoritems(this.tableOptions).pipe(takeUntil(this.ngUnsubscribe)).subscribe((res: any) => {
      if (res) {
        this.contractorData = res;
      }
      if (this.contractorData && this.contractorData.length)
        this.filteredContractorsMulti.next(this.contractorData.slice());
    }, error => {
      console.log(error);
    });
  }

  getDepartments() {
    this.departmentService.getDepartments(this.tableOptions).pipe(takeUntil(this.ngUnsubscribe)).subscribe((res: any) => {
      if (res) {
        this.departmentData = res;
      }
      if (this.departmentData && this.departmentData.length)
        this.filteredDepartmentMulti.next(this.departmentData.slice());
    }, error => {
      console.log(error);
    });
  }

  getUserDetails() {
    if (this.id) {
      this.userService.getUserDetails(this.id).pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((res) => {
          let resObj: any = res;
          if (resObj) {
            this.form.patchValue({
              id: resObj.id,
              name: resObj.name,
              email: resObj.username,
              phone: resObj.phone,
              designation: resObj.designation,
              roleIds: resObj.roleIds,
              departmentIds: resObj.departmentIds,
              address: resObj.address,
              gender: resObj.gender,
              contractorIds: resObj.contractorIds
            });
          }
        }, error => {
          // this.alertifyService.error(error);
        });
    }
  }

  onSave() {
    if (this.form.invalid) {
      // this.alertifyService.info('Please fill all required fields.');
      return;
    }
    if (this.form.value.id == null) {
      let model = this.form.value;
      model.username = model.email;
      delete model.id;
      this.userService.add(model).pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
        // this.alertifyService.success('User has been added successfully.', '/user');
      }, error => {
        var message = error && error.message ? error.message : 'Sorry, Something went wrong.';
        // this.alertifyService.error(message);
      });
    }
    else {
      let model = this.form.value;
      model.username = model.email;
      this.userService.update(this.form.value).pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
        // this.alertifyService.success('User has been updated successfully.', '/user');
      }, error => {
        var message = error && error.message ? error.message : 'Sorry, Something went wrong.';
        // this.alertifyService.error(message);
      });
    }

  }
  onCancel() {
    this.location.back();
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  private filterRolesMulti() {
    if (!this.roleData) {
      return;
    }
    // get the search keyword
    let search = this.roleMultiFilterCtrl.value;
    if (!search) {
      this.filteredRolesMulti.next(this.roleData.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the roles
    this.filteredRolesMulti.next(
      this.roleData.filter(role => role.name.toLowerCase().indexOf(search) > -1)
    );
  }
  //contractor
  private filterContractorsMulti() {
    if (!this.contractorData) {
      return;
    }
    // get the search keyword
    let search = this.contractorMultiFilterCtrl.value;
    if (!search) {
      this.filteredContractorsMulti.next(this.contractorData.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the contractors
    this.filteredContractorsMulti.next(
      this.contractorData.filter(contractor => contractor.name.toLowerCase().indexOf(search) > -1)
    );
  }


  private filterDepartmentMulti() {
    if (!this.departmentData) {
      return;
    }
    // get the search keyword
    let search = this.departmentMultiFilterCtrl.value;
    if (!search) {
      this.filteredDepartmentMulti.next(this.departmentData.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the roles
    this.filteredDepartmentMulti.next(
      this.departmentData.filter(dep => dep.name.toLowerCase().indexOf(search) > -1)
    );
  }


}
