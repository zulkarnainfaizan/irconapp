import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthenciationPageRoutingModule } from './authenciation-routing.module';

import { AuthenciationPage } from './authenciation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuthenciationPageRoutingModule
  ],
  declarations: [AuthenciationPage]
})
export class AuthenciationPageModule {}
