// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.page.html',
//   styleUrls: ['./login.page.scss'],
// })
// export class LoginPage implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { DepartmentService } from 'src/app/service/department/department.service';
import { GlobalUtil } from 'src/app/global-values/global-values';
import { ModuleService } from 'src/app/service/module/module.service';
import { AuthService } from 'src/app/service/auth/auth.service';
import { AlertifyService } from 'src/app/service/alerity/alertify.service';
import { AuthUtil } from 'src/app/global-values/auth-util';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
    @ViewChild('loginForm', { static: false }) public loginForm: NgForm;
    @ViewChild('videoPlayer', { static: false }) videoPlayer: ElementRef;
    loginModel: any = {};
    loginError: any = '';
    siteName: any = '';
    departments: any[] = [];
    tableOptions: any = {
        page: 1,
        rows: 15,
        isSearch: false,
        searchKeyword: "",
        SortCol: "DisplayOrder",
        IsAscending: true,
        selectedCols: []
    };
    constructor(private router: Router, private departmentService: DepartmentService,private moduleService: ModuleService, private authService: AuthService, private alertifyService: AlertifyService) {}

    ngOnInit() {
        this.authService.authorize();
        this.playVideo();
        this.siteName = environment.SiteName;
    }

    modelChanged() {
        this.loginError = '';
        this.playVideo();
    }

    playVideo() {
        try {
            this.videoPlayer.nativeElement["play()"];
        } catch (e) {

        }
    }

    login() {
        if (!this.loginForm.valid) {
            // this.alertifyService.info('Please fill in your credentials.');
            return;
        }
        else {
            this.alertifyService.showLoader();
            this.authService.login(this.loginModel).subscribe((res: any) => {
                this.alertifyService.hidePopup();
                if (res && res.user && res.token) {
                    AuthUtil.login(res);
                    this.getModules(() => {
                        this.router.navigate(['/home/dashboard']);
                    })
                }
                else {
                    // this.alertifyService.error('Incorrect username/password!');
                }
            }, error => {
                console.log(error);
                this.alertifyService.hidePopup();
                // this.alertifyService.error('Incorrect username/password!');
            });
        }
    }
    getModules(callback) {
        this.moduleService.getModuleList(this.tableOptions).subscribe((res: any) => {
            if (res) {
                GlobalUtil.setAllModules(res);
                callback();
            }
        }, error => {
            console.log(error);
            callback();
        });
    }
}

