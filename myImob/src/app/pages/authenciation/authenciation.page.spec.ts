import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuthenciationPage } from './authenciation.page';

describe('AuthenciationPage', () => {
  let component: AuthenciationPage;
  let fixture: ComponentFixture<AuthenciationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenciationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AuthenciationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
