import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenciationPage } from './authenciation.page';

const routes: Routes = [
  {
    path: '',
    component: AuthenciationPage
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenciationPageRoutingModule {}
